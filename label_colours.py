#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2023] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Label Colours-->
# An Inkscape 1.1+ extension-->
# Change Label Colours based on object properties -->
# Appears Under Extensions>Arrange>Label Colours-->
##############################################################################

import inkex
from random import randrange

def update_label_color(self, object_list, color_source):

        for item in object_list:

            if color_source == 'random':
                label_color = f'rgb({randrange(0, 255)},{randrange(0, 255)},{randrange(0, 255)})'

            else:
                object_style = item.specified_style()

                if color_source not in object_style.keys():
                    continue
                else:

                    if color_source != 'none':
                        label_color = object_style[color_source]
            item.set('inkscape:highlight-color', label_color)


class LabelColours(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--selection_source_radio", type=str, dest="selection_source_radio", default='document')
        pars.add_argument("--color_source_radio", type=str, dest="color_source_radio", default='selection')
    
    def effect(self):

        if self.options.selection_source_radio != 'document':

            selection_list = self.svg.selected
            if len(selection_list) < 1:
                inkex.errormsg('Nothing Selected')
                return

            if self.options.selection_source_radio == 'selection':

                object_list = selection_list

            else:
                object_list = []
                for item in selection_list:
                    children = item.xpath('.//svg:circle | .//svg:ellipse | .//svg:image | .//svg:line| .//svg:path |  .//svg:polygon | '
                                         './/svg:polyline | .//svg:rect | .//svg:text | .//svg:textPath | .//svg:title | '
                                         './/svg:tspan | .//svg:use')
                    for child in children:
                        object_list.append(child)

        else:

            object_list = self.svg.xpath('//svg:circle | //svg:ellipse | //svg:image | //svg:line| //svg:path |  //svg:polygon | '
                                         '//svg:polyline | //svg:rect | //svg:text | //svg:textPath | //svg:title | '
                                         '//svg:tspan | //svg:use')


        color_source = self.options.color_source_radio

        update_label_color(self, object_list, color_source)
        
if __name__ == '__main__':
    LabelColours().run()
